﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blabla.Core.Domain;

namespace Blabla.Core.Repositories
{
    interface IDailyRouteRepository : IRepository
    {
        Task<DailyRoute> GetAsync(Guid id);
        Task<IEnumerable<DailyRoute>> GetAllAsync();
        Task AddAsync(DailyRoute dailyRoute);
        Task UpdateAsync(DailyRoute dailyRoute);
        Task RemoveAsync(DailyRoute dailyRoute);
    }
}
