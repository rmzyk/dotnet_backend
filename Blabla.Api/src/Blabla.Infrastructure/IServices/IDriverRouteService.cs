﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.IServices
{
    public interface IDriverRouteService : IService
    {
        Task AddAsync(Guid userId, string name, double startLatitude, double startLongitude,
                      double endLatitude, double endLotitude);

        Task DeleteAsync(Guid userId, string name);
    }
}
