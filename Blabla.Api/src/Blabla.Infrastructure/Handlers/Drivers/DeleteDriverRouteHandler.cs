﻿using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Drivers;
using Blabla.Infrastructure.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Handlers.Drivers
{
    public  class DeleteDriverRouteHandler : ICommandHandler<DeleteDriverRoute>
    {
        private readonly IDriverRouteService _driverRouteService;

        public DeleteDriverRouteHandler(IDriverRouteService driverRouteService)
        {
            _driverRouteService = driverRouteService;
        }

        public async Task HandleAsync(DeleteDriverRoute command)
           => await _driverRouteService.DeleteAsync(command.UserId, command.Name);
    }
}
