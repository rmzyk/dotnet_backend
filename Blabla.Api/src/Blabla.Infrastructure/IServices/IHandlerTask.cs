﻿using Blabla.Core.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.IServices
{
    public interface IHandlerTask
    {
        IHandlerTask Always(Func<Task> always);

        IHandlerTask OnCustomError(Func<BlablaExceptions, Task> onCustomError,
            bool propagateException = false, bool excuteOnError = false);

        IHandlerTask OnError(Func<Exception, Task> onError,
            bool propagateException = false, bool excuteOnError = false);

        IHandlerTask OnSuccess(Func<Task> onSucces);

        IHandlerTask PropagateException();

        IHandlerTask DoNotPropagateException();

        IHandler Next();

        Task ExecuteAsync();
    }
}
