﻿using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Accounts;
using Blabla.Infrastructure.Extensions;
using Blabla.Infrastructure.IServices;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Handlers.Account
{
    class LoginHandler : ICommandHandler<Login>
    {
        private readonly IHandler _handler;
        private readonly IUserService _userService;
        private readonly IJwtHandler _jwtHandler;
        private readonly IMemoryCache _cache;

        public LoginHandler(IHandler handler, IUserService userService,
            IJwtHandler jwtHandler, IMemoryCache cache)
        {
            _handler = handler;
            _userService = userService;
            _jwtHandler = jwtHandler;
            _cache = cache;
        }

        public async Task HandleAsync(Login command)
            => await _handler
                .RunAsync(async () => await _userService.LoginAsync(command.Email, command.Password))
                .Next()
                .RunAsync(async () =>
                {
                    var user = await _userService.GetAsync(command.Email);
                    var jwt = _jwtHandler.CreateToken(user.Id, user.Role);
                    _cache.SetJwt(command.TokenId, jwt);
                })
                .ExecuteAsync();
    }
}
