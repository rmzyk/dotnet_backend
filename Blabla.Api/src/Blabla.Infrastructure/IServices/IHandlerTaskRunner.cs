﻿using System;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.IServices
{
    public interface IHandlerTaskRunner
    {
        IHandlerTask Run(Func<Task> run);
    }
}
