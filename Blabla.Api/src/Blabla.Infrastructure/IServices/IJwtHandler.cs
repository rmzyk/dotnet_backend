﻿using Blabla.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.IServices
{
    public interface IJwtHandler
    {
        JwtDto CreateToken(Guid userId, string Role);
    }
}
