﻿using Blabla.Api;
using Blabla.Infrastructure.Commands.Users;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Blabla.Tests.E2E.Controllers
{
   public class AccountControllerTest : ControllerTestsBase
    {
        
        [Fact]
        public async Task given_valid_password_should_allow_to_change_current_one()
        {
            var command = new ChangeUserPassword
            {
                CurrentPassword = "secret",
                NewPassword = "secret22"
            };

            var payload = GetPayload(command);
            var response = await Client.PutAsync("account/password", payload);
            response.StatusCode.Equals(HttpStatusCode.NoContent);
        }
    }
}
