﻿using Blabla.Infrastructure.Commands.Drivers.Models;
using System;

namespace Blabla.Infrastructure.Commands.Drivers
{
    public class CreateDriver : ICommand, IAuthenticatedCommand
    {
        public Guid UserId { get; set; }
        public DriverVehicle Vehicle { get; set; }
    }
}
