﻿using Blabla.Infrastructure.Commands.Users;
using Blabla.Infrastructure.DTO;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Blabla.Tests.E2E.Controllers
{
    public class UserControllerTests : ControllerTestsBase
    {
        [Fact]
        public async Task user_with_email_should_exists()
        {
            var email = "dupski1@dupa.com";
            var user = await this.GetUserAsync(email);
            Assert.Equal(user.Email, email); 
        }

        [Fact]
        public async Task user_with_email_should_NOT_exists()
        {
            var email = "dupski112@dupa.com";
            var response = await Client.GetAsync($"users/{email}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task user_should_be_created()
        {
            var request = new CreateUser
            {
                Email = "dupaaa@dupa.com",
                Username = "test",
                Password = "secret"
            };

            var payload = GetPayload(request);
            var response = await Client.PostAsync("users", payload);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            Assert.Equal(response.Headers.Location.ToString(), $"users/{request.Email}");

            var user = await GetUserAsync(request.Email);
            Assert.Equal(user.Email, request.Email);
        }

        private async Task<UserDto> GetUserAsync(string email)
        {
            var response = await Client.GetAsync($"users/{email}");
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<UserDto>(responseString);
        }
    }
}
