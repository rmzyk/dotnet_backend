﻿using Blabla.Core.Domain;
using Blabla.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.IServices
{
    public interface IVehicleProvider : IService
    {
        Task<IEnumerable<VehicleDto>> BrowseAsync();

        Task<VehicleDto> GetAsync(string brand, string name);
    }
}
