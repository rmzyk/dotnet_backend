﻿using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Drivers;
using Blabla.Infrastructure.IServices;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Handlers.Drivers
{
    public class CreateDriverRouteHandler : ICommandHandler<CreateDriverRoute>
    {
        private readonly IDriverRouteService _driverRouteService;

        public CreateDriverRouteHandler(IDriverRouteService driverRouteService)
        {
            _driverRouteService = driverRouteService;
        }
        public async Task HandleAsync(CreateDriverRoute command)
        {
            await _driverRouteService.AddAsync(command.UserId, command.Name, command.startLatitude,
                command.startLongitude, command.endLatitude, command.endLongitude);
        }
    }
}
