﻿CREATE DATABASE BlablaDB

USE BlablaDB

CREATE TABLE Users (
	Id uniqueidentifier primary key not null,
	Email nvarchar(100) not null,
	Password nvarchar(100) not null,
	Salt nvarchar(100) not null,
	Username nvarchar(100) not null,
	Fullname nvarchar(100) not null,
	Role nvarchar(100) not nulL,
	CreatedAt datetime not null,
	UpdatedAt datetime not null
	)