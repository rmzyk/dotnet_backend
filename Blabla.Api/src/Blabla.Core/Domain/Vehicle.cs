﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Core.Domain
{
    public class Vehicle
    {
        public string Name { get; protected set; }
        public int Seats { get; protected set; }
        public string Brand { get; protected set; }

        protected Vehicle() {}

        protected Vehicle(string brand, string name, int seats)
        {
            SetName(name);
            SetSeats(seats);
            SetBrand(brand);
        }

        private void SetBrand(string brand)
        {
            if (string.IsNullOrWhiteSpace(brand))
            {
                throw new Exception("Please provide valid data.");
            }

            if (Brand == brand)
            {
                return;
            }
            Brand = brand;
        }

        private void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Please provide valid data.");
            }

            if (Brand == name)
            {
                return;
            }

            Brand = name;
        }

        private void SetSeats(int seats)
        {
            if (seats<0)
            {
                throw new Exception("Please provide valid data.");
            }

            if (Seats == seats)
            {
                return;
            }

            Seats = seats;
        }

        public static Vehicle Create(string brand, string name, int seats) 
            => new Vehicle(brand, name, seats);
    }
}
