﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Drivers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blabla.Api.Controllers
{
    [Route("drivers/routes")]
    public class DriverRoutesController : ApiControllerBase
    {
        public DriverRoutesController(ICommandDispatcher commandDispatcher) 
            : base(commandDispatcher)
        {
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateDriverRoute command)
        {
            await DispatchAsync(command);

            return NoContent();
        }

        [Authorize]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            var command = new DeleteDriverRoute
            {
                Name = name
            };

            //await DispatchAsync(name);
        
            return NoContent();
        }
    }
}
