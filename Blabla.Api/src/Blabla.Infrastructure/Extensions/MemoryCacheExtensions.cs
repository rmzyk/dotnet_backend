﻿using Blabla.Infrastructure.DTO;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.Extensions
{
    public static class MemoryCacheExtensions
    {
        public static void SetJwt(this IMemoryCache memoryCache, Guid tokenId, JwtDto jwt)
            => memoryCache.Set(GetJwtKey(tokenId), jwt, TimeSpan.FromSeconds(10));

        public static JwtDto GetJwt(this IMemoryCache memoryCache, Guid tokenId)
            => memoryCache.Get<JwtDto>(GetJwtKey(tokenId));

        public static string GetJwtKey(Guid tokenId)
            => $"jwt-{tokenId}";
    }
}
