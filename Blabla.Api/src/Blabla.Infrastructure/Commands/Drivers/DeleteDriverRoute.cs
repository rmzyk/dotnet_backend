﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.Commands.Drivers
{
    public class DeleteDriverRoute : ICommand, IAuthenticatedCommand
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }
    }
}
