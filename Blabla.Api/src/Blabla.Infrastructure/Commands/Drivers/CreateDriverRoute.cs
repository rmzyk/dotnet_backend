﻿using Blabla.Infrastructure.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.Commands.Drivers
{
    public class CreateDriverRoute : ICommand, IAuthenticatedCommand
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }

        public double startLatitude { get; set; }

        public double startLongitude { get; set; }

        public double endLatitude { get; set; }

        public double endLongitude { get; set; }
    }
}
