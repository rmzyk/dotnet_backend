DOCKER_ENV=''
DOCKER_TAG=''
case "$TRAVIS_BRANCH" in
    "master")
        DOCKER_ENV=production
        DOCKER_TAG=latest
        ;;
    "develop")
        DOCKER_ENV=development
        DOCKER_TAG=develop
        ;;
esac

docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
docker build -f ./src/Blabla.Api/Dockerfile.$DOCKER_ENV -t blabla.api:$DOCKER_TAG ./src/Blabla.Api
docker tag blabla.api:$DOCKER_TAG $DOCKER_USERNAME/blabla.api:$DOCKER_TAG
docker push $DOCKER_USERNAME/blabla.api:$DOCKER_TAG