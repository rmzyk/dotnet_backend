﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Core.Domain
{
    public class Passanger
    {

            public Guid Id { get; protected set; }
            public Guid UserId { get; protected set; }
            public Node Address { get; protected set; }

            protected Passanger() { }

            public Passanger(Guid id, Guid userId, Node address)
            {
                Id = id;
                UserId = userId;
                Address = address;
            }
    }
}
