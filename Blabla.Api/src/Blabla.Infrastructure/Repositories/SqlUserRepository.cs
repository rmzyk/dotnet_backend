﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blabla.Core.Domain;
using Blabla.Core.Repositories;
using Blabla.Infrastructure.EntityFramework;
using Blabla.Infrastructure.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Blabla.Infrastructure.Repositories
{
    public class SqlUserRepository : IUserRepository, ISqlRepository
    {
        private readonly BlablaContext _context;

        public SqlUserRepository(BlablaContext context)
        {
            _context = context;
        }

        public async Task<User> GetAsync(Guid id)
            => await _context.Users.SingleOrDefaultAsync(x => x.Id == id);

        public async Task<User> GetAsync(string email)
            => await _context.Users.SingleOrDefaultAsync(x => x.Email == email);

        public async Task<IEnumerable<User>> BrowseAsync()
            => await _context.Users.ToListAsync();

        public async Task AddAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        } 

        public async Task UpdateAsync(User user)
        {
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(User user)
        {
            var userToDelete = await GetAsync(user.Id);
            await _context.Users.AddAsync(userToDelete);
            await _context.SaveChangesAsync();
        }
    }
}
