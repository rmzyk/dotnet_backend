﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blabla.Core.Domain;
using Blabla.Infrastructure.DTO;

namespace Blabla.Infrastructure.IServices
{
    public interface IUserService : IService
    {
        Task RegisterAsync(Guid userId, string username, string email, string password, string role);

        Task<IEnumerable<UserDto>> BrowseAsync();

        Task<UserDto> GetAsync(string email);

        Task LoginAsync(string email, string password);
    }
}
