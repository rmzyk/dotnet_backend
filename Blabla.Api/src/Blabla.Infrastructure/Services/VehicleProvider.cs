﻿using Blabla.Core.Domain;
using Blabla.Infrastructure.DTO;
using Blabla.Infrastructure.IServices;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Services
{
    public class VehicleProvider : IVehicleProvider
    {
        private readonly IMemoryCache _memoryCache;
        private readonly static string CacheKey = "vehicles";
        private static readonly IDictionary<string, IEnumerable<VehicleDetails>> availableVehicles
            = new Dictionary<string, IEnumerable<VehicleDetails>>()
            {
                ["Audi"] = new List<VehicleDetails>
                {
                    new VehicleDetails("A6", 6),
                    new VehicleDetails("A4", 4),
                    new VehicleDetails("A3", 4),
                    new VehicleDetails("A8", 3),
                    new VehicleDetails("A3", 4),
                    new VehicleDetails("A8", 3),
                    new VehicleDetails("A4 Quattro", 4),
                    new VehicleDetails("A6", 6)
                },

                ["Lexus"] = new List<VehicleDetails>
                {
                    new VehicleDetails("RC350", 4),
                    new VehicleDetails("RC350", 2),
                    new VehicleDetails("RC350", 2),
                    new VehicleDetails("RC340", 2),
                    new VehicleDetails("RC340", 8),
                    new VehicleDetails("RC330", 8),
                    new VehicleDetails("RC330", 8)
                },

                ["BMW"] = new List<VehicleDetails>
                {
                    new VehicleDetails("M4", 2),
                    new VehicleDetails("M4", 5),
                    new VehicleDetails("M5", 4),
                    new VehicleDetails("M5", 4),
                    new VehicleDetails("M5", 4),
                    new VehicleDetails("M4", 5),
                    new VehicleDetails("M4", 5),
                    new VehicleDetails("M4", 1),
                    new VehicleDetails("M4", 7)
                }
            };

        public VehicleProvider(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public async Task<IEnumerable<VehicleDto>> BrowseAsync()
        {
            var vehicles = _memoryCache.Get<IEnumerable<VehicleDto>>(CacheKey);
            if(vehicles is null)
            {
                vehicles = await GetAllAsync();
                _memoryCache.Set(CacheKey, vehicles);
            }

            return vehicles;
        }

        private async Task<IEnumerable<VehicleDto>> GetAllAsync()
            => await Task.FromResult(availableVehicles.GroupBy(x => x.Key)
                         .SelectMany(y => y.SelectMany(v => v.Value.Select(c => new VehicleDto
                         {
                             Brand  = v.Key,
                             Name = c.Name,
                             Seats = c.Seats
                         }))));

        public async Task<VehicleDto> GetAsync(string brand, string name)
        {
            if (!availableVehicles.ContainsKey(brand))
            {
                throw new Exception($"Vehicle brand: '{brand}' is not available.");
            }
            var vehicles = availableVehicles[brand];
            var vehicle = vehicles.SingleOrDefault(x => x.Name == name);
            if(vehicle == null)
            {
                throw new Exception($"Vehicle: '{name}' for brand: '{brand}' is not available.");
            }

            return await Task.FromResult(new VehicleDto
            {
                Brand = brand,
                Name = vehicle.Name,
                Seats = vehicle.Seats
            });
        }

        public class VehicleDetails
        {
            public string Name { get; }

            public int Seats { get; }


            public VehicleDetails(string name, int seats)
            {
                Seats = seats;
                Name = name;
            }
        }
    }
}
