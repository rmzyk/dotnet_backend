﻿using System;
using System.Collections.Generic;
using System.Text;
using Blabla.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Blabla.Infrastructure.EntityFramework
{
    public class BlablaContext : DbContext
    {
        private readonly SqlSettings _sqlSettings;

        public BlablaContext(DbContextOptions<BlablaContext> options,
            SqlSettings sqlSettings)
        :base(options)
        {
            _sqlSettings = sqlSettings;
        }

        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_sqlSettings.IsInMemoryDB)
            {
                optionsBuilder.UseInMemoryDatabase();
                return;
            }

            optionsBuilder.UseSqlServer(_sqlSettings.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userBuilder = modelBuilder.Entity<User>();
            userBuilder.HasKey(x => x.Id);
        }
    }
}
