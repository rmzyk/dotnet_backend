﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blabla.Infrastructure.DTO;

namespace Blabla.Infrastructure.IServices
{
   public interface IDriverService : IService
    {
        Task<DriverDetailsDto> GetAsync(Guid userId);
        Task<IEnumerable<DriverDto>> BrowseAsync();

        Task RegisterAsync(DriverDto driver);

        Task CreateAsync(Guid userId);

        Task SetVehicleAsync(Guid userId, string brand, string name);
        Task DeleteAsync(Guid userId);
    }
}
