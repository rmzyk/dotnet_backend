﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Core.Domain
{
    public class PassangerNode
    {
        public Node Node { get; protected set; }
        public Passanger Passanger { get; protected set; }

        protected PassangerNode() { }

        public PassangerNode(Node node, Passanger passanger)
        {
            Node = node;
            Passanger = passanger;
        }

        public static PassangerNode Create(Node node, Passanger passanger) => new PassangerNode(node, passanger);
    }
}
