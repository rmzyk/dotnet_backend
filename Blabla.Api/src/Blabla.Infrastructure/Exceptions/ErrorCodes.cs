﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.Exceptions
{
    public static class ErrorCodes
    {
        public static string EmailInUse => "email_in_use";

        public static string InvalidEmail => "invalid_email";

        public static string InvalidCredentials => "inalid_credentials";

        public static string DriverNotFound => "driver_not_found";

        public static string UserNotFound => "user_not_found";

        public static string DriverAlreadyExists => "driver_already_exists";
    }
}
