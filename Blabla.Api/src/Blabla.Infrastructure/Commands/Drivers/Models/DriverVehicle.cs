﻿namespace Blabla.Infrastructure.Commands.Drivers.Models
{
    public class DriverVehicle
    {
        public string Name { get; set; }

        public string Brand { get; set; }
    }
}
