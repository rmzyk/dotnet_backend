﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Blabla.Core.Domain
{
    public class Node
    {
        public string Address { get; protected set; }
        public double Longitude { get; protected set; }
        public double Latitude { get;protected set; }
        public DateTime UpdatedAt { get; protected set; }

        private static readonly Regex NameRegex = new Regex("^(?![_.-])(?~.*[_.-]{2})[a-zA-Z0-9._.-]+(?<![_.-])$");

        protected Node() { }

        protected Node(string address, double longitude, double latitude)
        {
            this.SetAddress(address);
            this.SetLatitude(latitude);
            this.SetLongitude(longitude);
            
        }

        public static Node Create(string address, double longitude, double latitude)
            => new Node(address, longitude, latitude);

        public void SetAddress(string address)
        {
            if (!NameRegex.IsMatch(address))
            {
                throw new Exception("Address is invalid.");
            }

            Address = address;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetLongitude(double longitude)
        {
            if (longitude < -180 || longitude > 180)
            {
                throw new Exception("Lognitude is invalid");
            }

            Longitude = longitude;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetLatitude(double latitude)
        {
            if (latitude < -90 || latitude > 90)
            {
                throw new Exception("Lognitude is invalid");
            }
            if (Latitude == latitude)
                return;

            Latitude = Latitude;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
