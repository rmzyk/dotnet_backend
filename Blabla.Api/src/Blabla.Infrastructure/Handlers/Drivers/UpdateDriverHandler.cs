﻿using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Drivers;
using Blabla.Infrastructure.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Handlers.Drivers
{
    public class UpdateDriverHandler : ICommandHandler<UpdateDriver>
    {
        private readonly IDriverService _driverService;

        public UpdateDriverHandler(IDriverService driverService)
        {
            _driverService = driverService;
        }

        public async Task HandleAsync(UpdateDriver command)
                => await _driverService.SetVehicleAsync(command.UserId, command.Vehicle.Brand,
                    command.Vehicle.Name);
    }
}
