﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Blabla.Core.Domain;
using Blabla.Core.Repositories;
using Blabla.Infrastructure.DTO;
using Blabla.Infrastructure.Extensions;
using Blabla.Infrastructure.IServices;

namespace Blabla.Infrastructure.Services
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IVehicleProvider _vehicleProvider;

        public DriverService(IDriverRepository driverRepository, IUserRepository userRepository,
            IMapper mapper, IVehicleProvider vehicleProvider)
        {
            _driverRepository = driverRepository;
            _vehicleProvider = vehicleProvider;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<DriverDetailsDto> GetAsync(Guid userId)
        {
            var driver = await _driverRepository.GetAsync(userId);

            return _mapper.Map<DriverDetailsDto>(driver);
        }

        public async Task RegisterAsync(DriverDto driver)
        {
            var repoDriver =  await _driverRepository.GetAsync(driver.userId);
            if (repoDriver != null)
            {
                throw new Exception($"User with id: '{driver.userId}' already exists.");
            }
            await _driverRepository.AddAsync(repoDriver);
        }

        public async Task SetVehicleAsync(Guid userId, string brand, string name)
        {
            var driver = await _driverRepository.GetOrFailAsync(userId);
            var vehicleDetails = await _vehicleProvider.GetAsync(brand, name);
            var vehicle = Vehicle.Create(brand, name, vehicleDetails.Seats);
            driver.SetVehicle(vehicle);
        }

        public async Task CreateAsync(Guid userId)
        {
            var user = await _userRepository.GetOrFailAsync(userId);
            var driver = await _driverRepository.GetAsync(userId);
            if (driver != null)
            {
                throw new Exception($"Driver with id: '{userId}' already exists.");
            }
            driver = new Driver(user);
            await _driverRepository.AddAsync(driver);
        }

        public async Task<IEnumerable<DriverDto>> BrowseAsync()
        {
            var drivers = await _driverRepository.BrowseAsync();
            return _mapper.Map<IEnumerable<Driver>, IEnumerable<DriverDto>>(drivers);
        }

        public async Task DeleteAsync(Guid userId)
          => await _driverRepository.DeleteAsync(await _driverRepository.GetOrFailAsync(userId));
    }
}
