﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Core.Domain.Exceptions
{
   public abstract class BlablaExceptions : Exception
   {
        public string Code { get; }

        protected BlablaExceptions()
        {
        }

        public BlablaExceptions(string code)
        {
            Code = code;
        }

        public BlablaExceptions(string message, params object[] args) : this(string.Empty, message, args)
        {
        }

        public BlablaExceptions(string code, string message, params object[] args) : this(null, code, message, args)
        {
        }

        public BlablaExceptions(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public BlablaExceptions(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message,args), innerException)
        {
            Code = code;
        }
    }
}
