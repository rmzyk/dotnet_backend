﻿using System.Threading.Tasks;

namespace Blabla.Infrastructure.IServices
{
    public interface IDataInitializer : IService
    {
       Task SeedAsync();
    }
}
