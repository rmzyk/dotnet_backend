﻿using Autofac;
using Blabla.Infrastructure.EntityFramework;
using Blabla.Infrastructure.Extensions;
using Blabla.Infrastructure.Migrations.MongoDB;
using Blabla.Infrastructure.Settings;
using Microsoft.Extensions.Configuration;

namespace Blabla.Infrastructure.IoC.Modules
{
    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterInstance(_configuration.GetSettings<GeneralSettings>())
                   .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<JwtSettings>())
                   .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<MongoSettings>())
                   .SingleInstance();
            builder.RegisterInstance(_configuration.GetSettings<SqlSettings>())
                   .SingleInstance();
        }
    }
}
