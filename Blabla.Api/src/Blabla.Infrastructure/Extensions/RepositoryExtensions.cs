﻿using Blabla.Core.Domain;
using Blabla.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Extensions
{
    public static class RepositoryExtensions
    {
        public static async Task<Driver> GetOrFailAsync(this IDriverRepository repository, Guid userId)
        {
            var driver = await repository.GetAsync(userId);
            if (driver.Equals(null))
            {
                throw new Exception($"Driver with id: '{userId}' was not found.");
            }

            return driver;
        }

        public static async Task<User> GetOrFailAsync(this IUserRepository repository, Guid userId)
        {
            var user = await repository.GetAsync(userId);
            if (user.Equals(null))
            {
                throw new Exception($"User with id: '{userId}' was not found.");
            }

            return user;
        }
    }
}
