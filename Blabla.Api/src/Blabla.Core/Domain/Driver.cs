﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blabla.Core.Domain
{
    public class Driver
    {
        private HashSet<Route> _routes;

        public Guid UserId { get; protected set; }
        public string Name { get; set; }
        public Vehicle Vehicle { get; protected set; }
        public IEnumerable<Route> Routes {
            get => _routes;
            set
            {
                _routes = new HashSet<Route>(value);
            }
        }
        public IEnumerable<DailyRoute> DailyRoutes { get; protected set; }
        public DateTime UpdatedAt { get; private set; }

        protected Driver() { }

        public Driver(User user)
        {
            UserId = user.Id;
            Name = user.Username;
        }

        public void SetVehicle(Vehicle vehicle)
        {
            Vehicle = vehicle;
            UpdatedAt = DateTime.UtcNow;
        }

        public void AddRoute(string name, Node start, Node end, double distance)
        {
            var route = Routes.SingleOrDefault(x => x.Name == name);
            if (route.Equals(null))
            {
                throw new Exception($"Route with name: '{name}' already exists for driver: {Name}.");
            }
            if(distance < 0)
            {
                throw new Exception($"Route with name: '{name}' cannot have negative distance.");
            }
            _routes.Add(Route.Create(name, start, end, distance));
            UpdatedAt = DateTime.UtcNow;
        }

        public void DeleteRoute(string name)
        {
            var route = Routes.SingleOrDefault(x => x.Name == name);
            if (route.Equals(null))
            {
                throw new Exception($"Route with name: '{name}' for driver: {Name} was not found.");
            }
            _routes.Remove(route);
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
