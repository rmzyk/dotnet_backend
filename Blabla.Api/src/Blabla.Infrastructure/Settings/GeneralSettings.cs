﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.Settings
{
    public class GeneralSettings
    {
        public String Name { get; set; }
        public bool SeedData { get; set; }
    }
}
