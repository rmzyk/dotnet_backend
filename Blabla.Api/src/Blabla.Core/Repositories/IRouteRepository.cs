﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blabla.Core.Domain;

namespace Blabla.Core.Repositories
{
    public interface IRouteRepository : IRepository
    {
        Task<Route> GetAsync(Guid id);
        Task<Route> GetAsync(Node startOrEndNode);
        Task<IEnumerable<Route>> GetAllAsync();
        Task AddAsync(Route route);
        Task UpdateAsync(Route route);
        Task RemoveAsync(Route route);
    }
}
