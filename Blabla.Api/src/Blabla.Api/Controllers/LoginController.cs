﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Users;
using Blabla.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Blabla.Api.Controllers
{
    public class LoginController : ApiControllerBase
    {
        private readonly IMemoryCache _memoryCache;

        public LoginController(ICommandDispatcher commandDispatcher, IMemoryCache memoryCache) 
            : base(commandDispatcher)
        {
            _memoryCache = memoryCache;
        }

        public async Task<IActionResult> Post([FromBody]Login command)
        {
            command.TokenId = Guid.NewGuid();
            await DispatchAsync(command);
            var jwt = _memoryCache.GetJwt(command.TokenId);

            return Json(jwt);
        }
    }
}
