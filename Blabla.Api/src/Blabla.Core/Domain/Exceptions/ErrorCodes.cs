﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Core.Domain.Exceptions
{
    public static class ErrorCodes
    {
        public static string InvalidUsername => "Invalid_username.";

        public static string InvalidEmail => "Invalid_email.";

        public static string InvalidRole => "Invalid_role.";

        public static string InvalidPassword => "Invalid_password.";
    }
}
