﻿using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Drivers;
using Blabla.Infrastructure.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Handlers.Drivers
{
    public class DeleteDriverHandler : ICommandHandler<DeleteDriver>
    {
        private readonly IDriverService _driverService;

        public DeleteDriverHandler(IDriverService driverService)
        {
            _driverService = driverService;
        }
        public async Task HandleAsync(DeleteDriver command)
               => await _driverService.DeleteAsync(command.UserId);
    }
}
