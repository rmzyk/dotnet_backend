﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.EntityFramework
{
    public class SqlSettings
    {
        public string ConnectionString { get; set; }

        public bool IsInMemoryDB { get; set; }
    }
}
