﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blabla.Core.Domain
{
    public class DailyRoute
    {
        private ISet<PassangerNode> _passangerNodes = new HashSet<PassangerNode>();
        public Guid Id { get; protected set; }
        public Route Route { get; protected set; }
        public IEnumerable<PassangerNode> PassangerNodes { get; protected set; }

        protected DailyRoute() { }

        public DailyRoute(Guid id, Route route, IEnumerable<PassangerNode> passangerNodes)
        {
            Id = id;
            Route = route;
            PassangerNodes = passangerNodes;
        }

        public void AddPassangerNode(Passanger passanger, Node node)
        {
            var passangerNode = this.GetPassangerNode(passanger);
            if(passangerNode is PassangerNode)
            {
                throw new Exception($"Node already exists for {passanger}.");
            }
            _passangerNodes.Add(PassangerNode.Create(node, passanger));
        }

        public void RemovePassangerNode(Passanger passanger)
        {
            var passengerNode = this.GetPassangerNode(passanger);
            if(passengerNode is null)
            {
                throw new Exception($"{passengerNode} does not exists.");
            }
            this._passangerNodes.Remove(passengerNode);
        }

        private PassangerNode GetPassangerNode(Passanger passanger) =>
            _passangerNodes.FirstOrDefault(x => x.Passanger == passanger);
    }
}
