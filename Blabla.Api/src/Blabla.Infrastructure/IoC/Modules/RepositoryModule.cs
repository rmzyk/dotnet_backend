﻿using Autofac;
using Blabla.Core.Repositories;

namespace Blabla.Infrastructure.IoC.Modules
{
   public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var assembly = typeof(RepositoryModule)
                                .GetType()
                                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IRepository>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
