﻿using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Users;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.Handlers.Users
{
    public class ChangeUserPasswordHandler : ICommandHandler<ChangeUserPassword>
    {
        public async Task HandleAsync(ChangeUserPassword command)
        {
            await Task.CompletedTask;
        }
    }
}
