﻿using Blabla.Infrastructure.Commands.Drivers.Models;

namespace Blabla.Infrastructure.Commands.Drivers
{
    public class UpdateDriver : AuthenticatedCommandBase
    {
        public DriverVehicle Vehicle { get; set; }
    }
}
