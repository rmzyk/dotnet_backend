﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.DTO
{
  public class VehicleDto
    {
        public string Brand { get; set; }

        public string Name { get; set; }

        public int Seats { get; set; }
    }
}
