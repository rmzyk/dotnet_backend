﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blabla.Core.Domain;

namespace Blabla.Core.Repositories
{
    public interface IPassangerRepository : IRepository
    {
        Task<Passanger> GetAsync(Guid id);
        Task<Passanger> GetAsync(string address);
        Task<IEnumerable<Passanger>> GetAllAsync();
        Task AddAsync(Passanger passanger);
        Task UpdateAsync(Passanger passanger);
        Task RemoveAsync(Passanger passanger);
    }
}
