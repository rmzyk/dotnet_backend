﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Blabla.Infrastructure.IServices
{
    public interface IHandler : IService
    {
        IHandlerTask RunAsync(Func<Task> run);

        IHandlerTaskRunner Validate(Func<Task> validate);

        Task ExecuteAllAsync();
    }
}
