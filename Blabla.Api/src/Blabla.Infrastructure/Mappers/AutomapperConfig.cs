﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Blabla.Core.Domain;
using Blabla.Infrastructure.DTO;

namespace Blabla.Infrastructure.Mappers
{
    public static class AutomapperConfig
    {
        public static IMapper Initialize()
            => new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<DailyRoute, DailyRouteDto>();
                    cfg.CreateMap<Driver, DriverDetailsDto>();
                    cfg.CreateMap<Driver, DriverDto>();
                    cfg.CreateMap<Node, NodeDto>();
                    cfg.CreateMap<Passanger, PassangerDto>();
                    cfg.CreateMap<PassangerNode, PassengerNodeDto>();
                    cfg.CreateMap<Route, RouteDto>();
                    cfg.CreateMap<User, UserDto>();
                    cfg.CreateMap<Vehicle, VehicleDto>();
                })
                .CreateMapper();
    }
}
