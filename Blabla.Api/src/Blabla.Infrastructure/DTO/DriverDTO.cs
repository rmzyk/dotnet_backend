﻿using System;
using System.Collections.Generic;
using System.Text;
using Blabla.Core.Domain;

namespace Blabla.Infrastructure.DTO
{
   public class DriverDto
   {
        public Guid userId { get; set; }
        public string Name { get; set; }
        public DateTime UpdatedAt { get; set; }
        public VehicleDto Vehicle { get; set; }
   }
}
