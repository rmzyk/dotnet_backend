﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Blabla.Api.Controllers 
{
    public class VehiclesController : ApiControllerBase
    {
        private readonly IVehicleProvider _vehicleProvider;
        public VehiclesController(ICommandDispatcher commandDispatcher, IVehicleProvider vehicleProvider) 
            : base(commandDispatcher)
        {
            _vehicleProvider = vehicleProvider;
        }

        public async Task<IActionResult> Get()
        {
            var vehicles = await _vehicleProvider.BrowseAsync();
            return Json(vehicles);
        }
    }
}
