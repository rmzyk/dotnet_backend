﻿using Blabla.Core.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Blabla.Core.Domain
{
    public class User
    {
        private static readonly Regex NameRegex = new Regex("/^[a-zA-Z]+[s|-]?[a-zA-Z]+[s|-]?[a-zA-Z]+$/");
        public Guid Id { get; protected set; }
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public string Salt { get; protected set; }
        public string Username { get; protected set; }
        public string FullName { get; protected set; }
        public string Role { get; protected set; }
        public DateTime CreatedAt { get; protected set; }
        public DateTime UpdatedAt { get; protected set; }

        protected User()
        {
        }

        public User(Guid userId, string email, string username, string password, string salt, string role)
        {
            Id = userId;
            Email = email.ToLowerInvariant();
            this.SetUsername(username);
            this.SetPassword(password);
            Salt = salt;
            CreatedAt = DateTime.UtcNow;
            Role = role;
        }

        public void SetUsername(string username)
        {
            if (NameRegex.IsMatch(username))
            {
                throw new DomainException(ErrorCodes.InvalidUsername, "Username is invalid.");
            }
            Username = username;
            UpdatedAt = DateTime.UtcNow;
        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new DomainException(ErrorCodes.InvalidPassword, "password is invalid.");
            }
            if (password.Length < 4)
            {
                throw new DomainException(ErrorCodes.InvalidPassword, "password is invalid.");
            }
            if (password.Length > 25)
            {
                throw new DomainException(ErrorCodes.InvalidPassword, "password is invalid.");
            }
            Password = password;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}
