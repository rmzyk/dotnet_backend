﻿using Autofac;
using Blabla.Infrastructure.IServices;
using Blabla.Infrastructure.Services;

namespace Blabla.Infrastructure.IoC.Modules
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            var assembly = typeof(ServiceModule)
                                .GetType()
                                .Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(x => x.IsAssignableTo<IService>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<Encrypter>().As<IEncrypter>().SingleInstance();

            builder.RegisterType<JwtHandler>().As<IJwtHandler>().SingleInstance();
        }
    }
}
