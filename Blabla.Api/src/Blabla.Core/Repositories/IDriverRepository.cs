﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Blabla.Core.Domain;

namespace Blabla.Core.Repositories
{
    public interface IDriverRepository: IRepository
    {
        Task<Driver> GetAsync(Guid userId);
        Task<Driver> GetAsync(IEnumerable<Route> routes);
        Task<Driver> GetAsync(IEnumerable<DailyRoute> dailyRoutes);
        Task<IEnumerable<Driver>> BrowseAsync();
        Task AddAsync(Driver driver);
        Task UpdateAsync(Driver driver);
        Task RemoveAsync(Driver driver);
        Task DeleteAsync(Driver driver);
    }
}