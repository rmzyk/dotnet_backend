﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blabla.Core.Domain;
using Blabla.Core.Repositories;

namespace Blabla.Infrastructure.Repositories
{
    public class DriverRepository : IDriverRepository
    {
        private static ISet<Driver> _drivers = new HashSet<Driver>();

        public async Task AddAsync(Driver driver)
        {
            _drivers.Add(driver);
            await Task.CompletedTask;
        }

        public async Task<IEnumerable<Driver>> BrowseAsync()
                => await Task.FromResult(_drivers);

        public async Task DeleteAsync(Driver driver)
        {
            _drivers.Remove(driver);
            await Task.CompletedTask;
        }

        public async Task<Driver> GetAsync(Guid userId)
                => await Task.FromResult(_drivers.Single(x => x.UserId == userId));

        public async Task<Driver> GetAsync(IEnumerable<Route> routes)
                => await Task.FromResult(_drivers.Single(x => x.Routes == routes));

        public async Task<Driver> GetAsync(IEnumerable<DailyRoute> dailyRoutes)
                => await Task.FromResult(_drivers.Single(x => x.DailyRoutes == dailyRoutes));

        public async Task RemoveAsync(Driver driver)
                => await Task.FromResult(_drivers.Remove(driver));

        public async Task UpdateAsync(Driver driver)
                => await Task.CompletedTask;
    }
}
