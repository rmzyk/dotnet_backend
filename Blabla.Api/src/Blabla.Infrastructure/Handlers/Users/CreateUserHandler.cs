﻿using System.Threading.Tasks;
using Blabla.Infrastructure.Commands;
using Blabla.Infrastructure.Commands.Users;
using Blabla.Infrastructure.IServices;

namespace Blabla.Infrastructure.Handlers.Users
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        private readonly IUserService _userService;

        public CreateUserHandler(IUserService userService)
        {
            _userService = userService;
        }
        public async Task HandleAsync(CreateUser command)
        {
            await _userService.RegisterAsync(command.UserId, command.Email, command.Username, command.Password, command.Role);
        }
    }
}
