﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blabla.Infrastructure.DTO
{
    public class DriverDetailsDto : DriverDto
    {
        public IEnumerable<RouteDto> Routes { get; set; }
        public IEnumerable<DailyRouteDto> DailyRoutes { get; set; }
    }
}
